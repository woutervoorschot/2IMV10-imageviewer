using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrimeMeridian : MonoBehaviour
{
    private static PrimeMeridian _S;
    static public PrimeMeridian S
    {
        get
        {
            if (_S == null)
            {
                Debug.LogError("PrimeMeridian:S getter - Attempt to get value of S before it has been set.");
                return null;
            }
            return _S;
        }
        private set
        {
            if (_S != null)
            {
                Debug.LogError("PrimeMeridian:S setter - Attempt to set S when it has already been set.");
            }
            _S = value;
        }
    }


    public Transform latUpperLimit;
    public Transform latLowerLimit;
    public Transform longLeftLimit;
    public Transform longRightLimit;


    /// <summary>
    /// Gets the latitude position in the map
    /// </summary>
    /// <param name="degrees"></param>
    /// <param name="minutes"></param>
    /// <param name="seconds"></param>
    /// <returns></returns>
    public float GetLatitude(double degrees, double minutes, double seconds)
    {
        double latitude = degrees + minutes / 60 + seconds / 3600;

        float lat = (float)latitude;
        float dist = 0;
        if (latitude > 0)
        {
            dist = Vector3.Distance(transform.position, latUpperLimit.position);
            dist = transform.position.y + (dist / 90) * lat;
        }
        else
        {
            dist = Vector3.Distance(transform.position, latLowerLimit.position);
            dist = transform.position.y - (dist / 90) * lat;
        }

        return dist;
    }

    /// <summary>
    /// Gets the longitude position in the map
    /// </summary>
    /// <param name="degrees"></param>
    /// <param name="minutes"></param>
    /// <param name="seconds"></param>
    /// <returns></returns>
    public float GetLongitude(double degrees, double minutes, double seconds)
    {
        double longitude = degrees + minutes / 60 + seconds / 3600;

        float longit = (float)longitude;
        float dist = 0;
        if (longit > 0)
        {
            dist = Vector3.Distance(transform.position, longRightLimit.position);
            dist = transform.position.x + (dist / 180) * longit;
        }
        else
        {
            dist = Vector3.Distance(transform.position, longLeftLimit.position);
            dist = transform.position.x - (dist / 180) * longit;
        }

        return dist;
    }


    private void Awake()
    {
        if (_S != null)
        {
            Debug.Log("PrimeMeridian: Only one instance can exist at any time. Destroying potential usurper.", _S.gameObject);
            Destroy(gameObject);
            return;
        }
        else
        {
            S = this;
        }
    }
}
