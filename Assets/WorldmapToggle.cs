using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof (Toggle))]
public class WorldmapToggle : MonoBehaviour
{

    public Image worldMap;

    void Start()
    {
        GetComponent<Toggle>().onValueChanged.AddListener((value) => { GameManager.S.ToggleWorldMap(value); });
        GetComponent<Toggle>().onValueChanged.AddListener((value) => { worldMap.enabled = value; });
    }
}
