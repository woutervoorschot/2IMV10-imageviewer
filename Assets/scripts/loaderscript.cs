
using Newtonsoft.Json;
using SFB;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class loaderscript : MonoBehaviour
{
    public Object prefab;
    public LoadAlbumAs loadAlbumAs = LoadAlbumAs.NONE;
    public float loadingInterval = 0.1f;
    private WaitForSeconds waitInverval;

    // Start is called before the first frame update
    private void Awake()
    {
        // This is now added by the Pregab GameManager and it's transform gravityCenter.
        //attractedTo = new GameObject("GravityCenter");
        //Rigidbody2D gameObjectsRigidBody = attractedTo.AddComponent<Rigidbody2D>(); // Add the rigidbody.
        //gameObjectsRigidBody.mass = 5; // Set the GO's mass to 5 via the Rigidbody.
        //attractedTo.transform.position = new Vector3(0, 0, 0);
        //attractedTo.transform.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Static;

        gameObject.GetComponent<Button>().onClick.AddListener(() => { StartCoroutine(GetAlbum()); });
        waitInverval = new WaitForSeconds(loadingInterval);
    }

    public static void Create(Object prefab, string url, float maxSize)
    {
        var Image = Instantiate(prefab, GameManager.S.photoHolder) as GameObject;
        // Retrieve the script from the GameObject
        loadsingleimage loadsingleimage = Image.GetComponent<loadsingleimage>();
        loadsingleimage.LoadImage(url);
        Image.GetComponent<dragscript>().maxSize = maxSize;

    }

    IEnumerator GetAlbum()
    {
        // remove existing images
        foreach(GameObject g in GameObject.FindGameObjectsWithTag("image"))
        {
            Destroy(g);
        }


        switch (loadAlbumAs)
        {
            case LoadAlbumAs.WEB:
                // load from internet
                UnityWebRequest www = UnityWebRequest.Get("https://esrtheta.nl/api/v1/photoalbum/5fb6eb825219ca00c0ea8868");
                yield return www.SendWebRequest();

                if (www.result != UnityWebRequest.Result.Success)
                {
                    Debug.Log(www.error);
                }
                else
                {
                    // Show results as text
                    Album _ = Album.CreateFromJSON(www.downloadHandler.text);
                    int count = _.uploads.Count();
                    float size = 1.666667f - 0.01065574f * count + 0.00002239541f * (count ^ 2);
                    foreach (Upload u in _.uploads)
                    {
                        if (loadingInterval > 0)
                        {
                            yield return (waitInverval);
                        }
                        Create(prefab, u.url, size);
                    }
                }

             break;

            case LoadAlbumAs.LOCAL:
                // open files from folder
                var extensions = new[] {new ExtensionFilter("Image Files", "jpg", "jpeg" )};

                var paths = StandaloneFileBrowser.OpenFilePanel("Open File", "", extensions, true);

                if (paths.Length > 0)
                {
                    // var urlArr = new List<string>(paths.Length);
                    int count = paths.Length;
                    // StartCoroutine(OutputRoutine(new System.Uri(paths[0]).AbsoluteUri));
                    float size = 1.666667f - 0.01065574f * count + 0.00000239541f * (count ^ 2);
                    for (int i = 0; i < paths.Length; i++)
                    {
                        if (loadingInterval > 0)
                        {
                            yield return (waitInverval);
                        }
                        // urlArr.Add(new System.Uri(paths[i]).AbsoluteUri);
                        Create(prefab, new System.Uri(paths[i]).AbsoluteUri, size);
                    }
                }

                break;
        }
    }
    public XmlDocument ExtractXmp(byte[] jpegBytes)
    {
        var asString = Encoding.UTF8.GetString(jpegBytes);
        var start = asString.IndexOf("<x:xmpmeta");
        var end = asString.IndexOf("</x:xmpmeta>") + 12;
        if (start == -1 || end == -1)
            return null;
        var justTheMeta = asString.Substring(start, end - start);
        var returnVal = new XmlDocument();
        returnVal.LoadXml(justTheMeta);
        return returnVal;
    }

}

[System.Serializable]
public class Album
{
    public string title;
    public List<Upload> uploads;
    public System.DateTime album_date;

    public static Album CreateFromJSON(string jsonString)
    {
        return JsonConvert.DeserializeObject<Album>(jsonString);
    }
}

public class Upload
{
    public string url { get { return "https://www.esrtheta.nl" + file; } }
    public string file { get; set; }
}

/// <summary>
/// Options on how to load an album
/// </summary>
public enum LoadAlbumAs 
{
    WEB,
    LOCAL,
    NONE
}
