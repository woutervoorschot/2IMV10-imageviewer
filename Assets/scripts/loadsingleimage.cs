using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.Networking;
using System.Threading.Tasks;
using System;
using System.Linq;
using System.Web;

public class loadsingleimage : MonoBehaviour
{

    public ExifLib.JpegInfo jpi;
    public bool hasValidGeoLocation;

    public void LoadImage(string url)
    {
        if (Uri.IsWellFormedUriString(url, UriKind.Absolute))
        {
            StartCoroutine(SetTexture(url));
        }
    }

    IEnumerator SetTexture(string url)
    {
        Texture2D texture = null;

        UnityWebRequest www = UnityWebRequest.Get(url);
        yield return www.SendWebRequest();

        if (www.result != UnityWebRequest.Result.Success)
        {
            Debug.Log(www.error);
        }
        else
        {
                
            // texture = ((DownloadHandlerTexture)www.downloadHandler).texture;
            var file = ((DownloadHandler)www.downloadHandler).data;
            jpi = ExifLib.ExifReader.ReadJpeg(file, Uri.UnescapeDataString(new Uri(url).Segments.Last()));


            double[] Latitude = jpi.GpsLatitude;
            double[] Longitude = jpi.GpsLongitude;

            hasValidGeoLocation = false;
            for (int i = 0; i < Latitude.Length; i++)
            {
                if(Latitude[i]!= 0)
                {
                    hasValidGeoLocation = true;
                    break;
                }
            }
            if (!hasValidGeoLocation)
            {
                for (int i = 0; i < Longitude.Length; i++)
                {
                    if (Longitude[i] != 0)
                    {
                        hasValidGeoLocation = true;
                        break;
                    }
                }
            }


            Texture2D tex = new Texture2D(2, 2);
            tex.LoadImage(file);
            tex = CorrectRotation(tex, jpi.Orientation);
            texture = tex;
        }
        
       

        float size = texture.width + texture.height;
        float height = (float)texture.height / size * 256f;
        float width = (float)texture.width / size * 256f;


        //image.Resize((int)Math.Ceiling(width), (int)Math.Ceiling(height));

        var render = gameObject.GetComponent<SpriteRenderer>();
        //render.sprite.max = new Vector2(3000f, 3000f);
        render.tileMode = SpriteTileMode.Continuous;
        render.sprite = Sprite.Create(texture,
            new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f), 100.0f);

        transform.localScale = new Vector3(0.2f, 0.2f);
        
        gameObject.GetComponent<BoxCollider2D>().size = render.sprite.bounds.extents * 2;
    }

    public Texture2D CorrectRotation(Texture2D tex, ExifLib.ExifOrientation orientationString)
    {
        // tries to use the jpi.Orientation to rotate the image properly
        
        switch (orientationString)
        {
            case ExifLib.ExifOrientation.TopRight: // Rotate clockwise 90 degrees
                return rotateTexture(tex, true);
            case ExifLib.ExifOrientation.TopLeft: // Rotate 0 degrees...
                return tex;
            case ExifLib.ExifOrientation.BottomRight: // Rotate clockwise 180 degrees
                tex = rotateTexture(tex, true);
                return  rotateTexture(tex, true);
            case ExifLib.ExifOrientation.BottomLeft: // Rotate clockwise 270 degrees (I think?)...
                return rotateTexture(tex, false);
            default:
                return tex;
        }

    }

    Texture2D rotateTexture(Texture2D originalTexture, bool clockwise)
    {
        Color32[] original = originalTexture.GetPixels32();
        Color32[] rotated = new Color32[original.Length];
        int w = originalTexture.width;
        int h = originalTexture.height;

        int iRotated, iOriginal;

        for (int j = 0; j < h; ++j)
        {
            for (int i = 0; i < w; ++i)
            {
                iRotated = (i + 1) * h - j - 1;
                iOriginal = clockwise ? original.Length - 1 - (j * w + i) : j * w + i;
                rotated[iRotated] = original[iOriginal];
            }
        }

        Texture2D rotatedTexture = new Texture2D(h, w);
        rotatedTexture.SetPixels32(rotated);
        rotatedTexture.Apply();
        return rotatedTexture;
    }
}