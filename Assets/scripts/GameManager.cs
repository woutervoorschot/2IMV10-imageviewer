using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private static GameManager _S;

    static public GameManager S
    {
        get
        {
            if (_S == null)
            {
                Debug.LogError("GameManager:S getter - Attempt to get value of S before it has been set.");
                return null;
            }
            return _S;
        }
        private set
        {
            if (_S != null)
            {
                Debug.LogError("GameManager:S setter - Attempt to set S when it has already been set.");
            }
            _S = value;
        }
    }

    /// <summary>
    /// Reference to the transform of the gravity Center for photos
    /// </summary>
    public Transform gravityCenter;

    /// <summary>
    /// Reference to the transform for the gravity points when world map is enabled
    /// </summary>
    public Transform worldGravityPoints_T;

    /// <summary>
    /// Photo holder so the hierarchy is clean. photos will be parented to this.
    /// </summary>
    public Transform photoHolder;

    #region Actions

    public static event System.Action<bool> toggleWorldMap;

    #endregion

    public void ToggleWorldMap(bool val)
    {
        toggleWorldMap(val);
    }



    private void Awake()
    {
        if (_S != null)
        {
            Debug.Log("GameManager: Only one instance can exist at any time. Destroying potential usurper.", _S.gameObject);
            Destroy(gameObject);
            return;
        }
        else
        {
            S = this;
        }
    }
}
