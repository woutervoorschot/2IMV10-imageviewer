using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]
public class dragscript : MonoBehaviour
{

    private Vector3 screenPoint;
    private Vector3 offset;
    private Quaternion targetRotation = Quaternion.Euler(0, 0, 0);
    public bool Growing = true;
    [SerializeField, Tooltip("The images cannot be smaller than this scale value")]
    private Vector3 minScale;
    public float maxSize;
    public float strengthOfAttraction = 0.5f;
    public bool rotationEnabled;

    [SerializeField]
    private loadsingleimage loadedImage;

    [Tooltip("Distance in which the direction will check if it's new direction won't make it collide with another instance.")]
    [SerializeField ] private float collisionDistance;
    [SerializeField] private LayerMask photosLayerMask;

    #region Variables
    private BoxCollider2D boxCollider2D;
    private Rigidbody2D rigidbody2D;
    private RaycastHit2D[] hits;
    private Transform currentGravityPoint;
    private Transform worldGravityPoint;
    #endregion

    public static event System.Action<bool> changeSize;

    void Start()
    {
        //gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(Random.Range(0.1f,0.3f), Random.Range(0.1f,0.3f));
        transform.position = new Vector3(Random.Range(-3f, 3f), Random.Range(-2f, 2f));

        currentGravityPoint = GameManager.S.gravityCenter;

        // subscribed actions
        changeSize += ChangeMaxSize;
        GameManager.toggleWorldMap += WorldMapToggled;

        // fill the cache variables
        boxCollider2D = GetComponent<BoxCollider2D>();
        rigidbody2D = GetComponent<Rigidbody2D>();
        loadedImage = GetComponent<loadsingleimage>();
    }
    private void OnDestroy()
    {
        changeSize -= ChangeMaxSize;
        GameManager.toggleWorldMap -= WorldMapToggled;
    }

    void OnMouseDown()
    {

        offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));
    }

    void OnMouseDrag()
    {
        Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);
        Vector3 curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint) + offset;
        transform.position = curPosition;
    }

    void Update()
    {
        // var targetRotation = Quaternion.Euler(0, 0, 0);
        if (rotationEnabled)
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, Time.deltaTime * 3.0f);
        }
        else
        {
            if (transform.rotation != Quaternion.identity)
            {
                transform.rotation = Quaternion.identity;
            }
        }
        // grow in size untill not fitting
        if (Growing && transform.localScale.magnitude < maxSize)
        {
            Vector3 newScale = Vector3.one;
            newScale.x = Mathf.Clamp(transform.localScale.x + 0.001f, minScale.x, transform.localScale.x);
            newScale.y = Mathf.Clamp(transform.localScale.y + 0.001f, minScale.y, transform.localScale.y);

            transform.localScale = newScale;
            // transform.localScale + new Vector3(.001f, .001f);
            // boxCollider2D.transform.localScale = transform.localScale;
        }


        Vector3 direction = Vector3.zero;
        if (currentGravityPoint != null)
        {
            direction = currentGravityPoint.position - transform.position;
        }

        // check if within next direction there would be a collision, to see if AddForce is necessary.

        hits = Physics2D.RaycastAll(transform.position, direction, collisionDistance, photosLayerMask);
        Vector3 target = transform.position + (direction * collisionDistance);
       // Debug.DrawLine(transform.position, target, Color.red, 0.1f);
        //Debug.DrawRay(transform.position, direction, Color.cyan, 0.1f);

        bool canAddForce = false;
        if (hits == null)
        {
            canAddForce = true;
        }
        else
        {
            for (int i = 0; i < hits.Length; i++)
            {
                if (hits[i].collider.gameObject.GetInstanceID() == gameObject.GetInstanceID())
                {
                    canAddForce = true;
                    continue;
                }
                else
                {
                    canAddForce = false;
                }
            }
        }
        if (canAddForce)
        {
            rigidbody2D.AddForce(strengthOfAttraction * direction);
        }
    }
    void OnCollisionEnter(Collision collision)
    {
        foreach (ContactPoint contact in collision.contacts)
        {
            Debug.Log("collide");
            Debug.DrawRay(contact.point, contact.normal, Color.white);
            if (contact.otherCollider.gameObject.tag.Equals("wall") || contact.otherCollider.gameObject.tag.Equals("image"))
            {
                Growing = false;
            }
        }
        //if (collision.relativeVelocity.magnitude > 2)
        //    audioSource.Play();
    }
    public void ChangeMaxSize(bool up)
    {
        float shrinkspeed = 0.01f;
        float growspeed = 0.01f;
        Vector3 newsize = Vector3.one;

        float scalar = up == true ? -shrinkspeed : growspeed;

        newsize.x = Mathf.Clamp(transform.localScale.x + scalar , minScale.x, transform.localScale.x);
        newsize.y = Mathf.Clamp(transform.localScale.y + scalar , minScale.y, transform.localScale.y);
       //newsize.x = Mathf.Clamp(transform.localScale.x + scalar * transform.localScale.x, minScale.x, maxSize);
       //newsize.y = Mathf.Clamp(transform.localScale.y + scalar * transform.localScale.y, minScale.y, maxSize);

        transform.localScale = newsize;
    }

    private void WorldMapToggled(bool toggle)
    {
        if (toggle)
        {
            if (loadedImage != null && loadedImage.hasValidGeoLocation)
            {
                if (worldGravityPoint == null)
                {
                    Vector3 magnetpos = Vector3.zero;
                    magnetpos.y = PrimeMeridian.S.GetLatitude(loadedImage.jpi.GpsLatitude[0], loadedImage.jpi.GpsLatitude[1], loadedImage.jpi.GpsLatitude[2]);
                    magnetpos.x = PrimeMeridian.S.GetLongitude(loadedImage.jpi.GpsLongitude[0], loadedImage.jpi.GpsLongitude[1], loadedImage.jpi.GpsLongitude[2]);

                    GameObject g = new GameObject("magnet " + "lat: " + magnetpos.x + ", long: " + magnetpos.y);
                    g.transform.SetParent(GameManager.S.worldGravityPoints_T);
                    g.transform.position = magnetpos;
                    worldGravityPoint = g.transform;
                }

                currentGravityPoint = worldGravityPoint;
            }
            else
            {
                currentGravityPoint = null;
            }
        }
        else
        {
            currentGravityPoint = GameManager.S.gravityCenter;
        }
    }

    private void OnMouseOver()
    {
        float scale = 30f * Input.GetAxis("Mouse ScrollWheel") * Time.deltaTime;
        //float scale = .4f * Input.GetAxis("Mouse ScrollWheel");
        if (scale != 0.0)
        {
            if (Input.GetKey(KeyCode.LeftShift) && rotationEnabled)
            {
                // rotate 
                targetRotation = Quaternion.Euler(0, 0, targetRotation.eulerAngles.z + scale * 180);
                // transform.rotation = targetRotation;
            }
            else
            {
                // scale
                Growing = false;
                // the object identified by hit.transform was clicked
                var temp = transform.localScale + new Vector3(scale, scale);
                
                maxSize = transform.localScale.x;

                // Call the action instead of looking for objects with tag and sending a message.
                changeSize(scale > 0);

                Vector3 newsize = Vector3.one;
                newsize.x = Mathf.Clamp(temp.x, minScale.x, temp.x);
                newsize.y = Mathf.Clamp(temp.y, minScale.y, temp.y);
                newsize.z = temp.z;
                transform.localScale = newsize;
            }
            
        }
    }
}