using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class toggleRotationScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        gameObject.GetComponent<Toggle>().onValueChanged.AddListener((value) => {
            foreach (GameObject g in GameObject.FindGameObjectsWithTag("image"))
            {
                g.GetComponent<dragscript>().rotationEnabled = value;
            }
        });

        

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
