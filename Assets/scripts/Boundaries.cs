using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boundaries : MonoBehaviour
{
    public Camera MainCamera;
    private Vector2 screenBounds;
    private float objectWidth;
    private float objectHeight;
    private dragscript dragscript;
    private SpriteRenderer spriteRenderer;
    // Use this for initialization
    void Start()
    {
        MainCamera = Camera.main;
        screenBounds = MainCamera.ScreenToWorldPoint(new Vector3(MainCamera.pixelWidth, MainCamera.pixelHeight, MainCamera.transform.position.z));
        dragscript = GetComponent<dragscript>();
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void LateUpdate()
    {
        // Get object width and height
        objectWidth = spriteRenderer.bounds.extents.x; //extents = size of width / 2
        objectHeight = spriteRenderer.bounds.extents.y; //extents = size of height / 2
        
        // Force object's extent to be within boundaries
        Vector3 viewPos = transform.position;
        // x
        if(viewPos.x < screenBounds.x * -1 + objectWidth)
        {
            // smaller in x
            viewPos.x = screenBounds.x * -1 + objectWidth;
            StopAllGrowing();
        }
        else if (viewPos.x > screenBounds.x - objectWidth)
        {
            // larger in x
            viewPos.x = screenBounds.x - objectWidth;
            StopAllGrowing();
        }
        if(viewPos.y < screenBounds.y * -1 + objectHeight)
        {
            viewPos.y = screenBounds.y * -1 + objectHeight;
            StopAllGrowing();
        }
        else if (viewPos.y > screenBounds.y - objectHeight)
        {
            viewPos.y = screenBounds.y - objectHeight;
            StopAllGrowing();
        }
        transform.position = viewPos;
    }

    void StopAllGrowing()
    {
        dragscript.Growing = false;
    }
}