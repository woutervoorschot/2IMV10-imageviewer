# Welcome

This repo hosts an interactive image viewer created in unity.

Created by Isilsu Keleş, Wouter van Oorschot and Jurjen Faber for the course 2IMV10 of the Eindhoven University of Technology, year 2020-2021

# Installation

The image viewer can be run on Windows by extracting the imageviewer.zip file and executing the imageviewer.exe file within it. This gives access to the full image viewer.

# Usage

When the application starts the images to be displayed have to be opened. The _Load from web_ button loads around 120 images from a recent rowing event in Eindhoven, as a demo set to use. The _Open files_ button lets the user select images from their own PC to display. When the images are loaded they will float to the center of the application window. The user can drag the images around with the mouse. With the mouse scroll wheel the images can be enlarged or shrunk. The other images in the viewer will shrink or grow accordingly so the used area of all images stays the same. By default the images will stay upright, but by checking the rotation checkbox the images can rotate around the z axis, they will still slowly rotate back to their upright, or user defined rotation angle. With shift and scroll the images can be rotated by the user.

New images can be loaded by the _Load from web_ or _Open files_ buttons, this will clear the current images from the view. 

# Build

To build and inspect the source, the gitlab repository \cite{gitlab} can be cloned. The scripts are located in `Assets/scripts/*.c#`, however to get the full overview of the source the unity editor should be installed. This can be downloaded from the [unity website](https://store.unity.com/download?ref=personal) When the project is opened in unity it can be run with the play button in the top center of the interface.
